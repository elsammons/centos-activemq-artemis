# Purpose #
This process create a docker image based on CentOS and install Apache ActiveMQ/Artemis 1.1.

# Current Known Issues #
* None known

# TO DO #
* Determine if it is possible to leverage host storage during the build process.
# Notes about My Configuration #
* I'm running with a bridged connection; br0 to my active ethernet.
* I use the bridge option on the docker daemon and specify my host's bridge.  Update your hosts's /etc/sysconfig/docker file to include.
```
OPTIONS="--selinux-enabled -b br0"
```
# Process #
1. Pull the docker 
> $ sudo docker pull elsammons/centos-activemq-artemis
> fb7e8103ffd: Pull
> a3c1bd245177: Pull
> 6236fa609159: Pull
> cca645ca24ab: Pull complete
> fb7ad89538ae: Pull complete
> Digest: sha256:nnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn
> Status: Downloaded newer image for docker.io/elsammons/centos-activemq-artemis:latest
1. View the docker images. 
> $ sudo docker images
> REPOSITORY                                    TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
> docker.io/elsammons/centos-activemq-artemis   latest              fb7ad89538ae        16 minutes ago      591.2 MB
> $ sudo docker run -d elsammons/centos-activemq-artemis
> a04e7ca51c93efd37f251cb4bc1040993cc96eb084a9fe5c2764d6d3f19d248c

## View the status of your Image ##
> $ sudo docker ps -l

## Stop Your Docker Image ##
> $ sudo docker stop <container id>

## To Run Your Docker Image ##
> $ sudo docker run -P --rm elsammons/centos-activemq-artemis

## To Inspect your Docker Image ##
> $ sudo docker inspect <container id>

# Optional Post Setup #
* I am also sharing local, host storage, with my running docker image.  I haven't yet figured out how to set this up in the dockerfile so at the moment I build my image, shut it down, and then copy the contents from the image to my host.  
    * Once the contents of the etc/ config/ and data/ directories are on my host I restart the docker image with the appropriate volume options.
    * You'll need the docker image of your currently running artemis docker image.
> sudo mkdir -p /var/lib/artemis
> docker run --rm -v /var/lib/artemis:/host:rw --volumes-from **a04e7ca51c93** centos:latest cp -r /var/lib/artemis/* /host/.