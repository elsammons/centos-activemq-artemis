##
# Name          : run-artemis.sh
# Author        : Eric L Sammons <elsammons@gmail.com>
##
# Check for the existence of the artemis broker
# and either create and run or simply run 
# accordingly.
##


if [ ! -d /artemis-broker ]; then
    ## Create the artemis broker, very basic, nothing fancy.
    ##cd /home/artemis
    echo ${PWD}
    ls -l .
    /opt/apache-artemis/bin/artemis create artemis-broker --home /opt/apache-artemis --user test --password test123 --allow-anonymous --role amq 
    ## Update some configuration options.
    ##cd /home/artemis/artemis-broker/etc 
    cd /artemis-broker/etc 
    echo ${PWD}
    xmlstarlet ed -L -N amq="http://activemq.org/schema" -u "/amq:broker/amq:web/@bind" -v "http://0.0.0.0:8161" bootstrap.xml

    xmlstarlet ed -L -N a="urn:activemq" -N c="urn:activemq:core" -s "/a:configuration/c:core" -t elem -n "security-enabled" -v "false" -s "/a:configuration/c:core" -t elem -n "queues" -s "/a:configuration/c:core/queues" -t elem -n "queue" -s "/a:configuration/c:core/queues/queue" -t attr -n "name" -v "test.queue" -s "/a:configuration/c:core/queues/queue" -t elem -n "address" -v "test.queue" -s "/a:configuration/c:core/queues/queue" -t elem -n "durable" -v "true" broker.xml && echo "xmlstarlet executed"

    cd /artemis-broker/bin 
    sed -i "s/logger.handlers=.*/logger.handlers=CONSOLE/g" ../etc/logging.properties && echo "logging properties updated"
fi
